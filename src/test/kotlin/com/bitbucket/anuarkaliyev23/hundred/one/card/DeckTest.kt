package com.bitbucket.anuarkaliyev23.hundred.one.card

import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class DeckTest {

    @Test
    fun constructorTest() {
        val deck = Deck()
        assertTrue { deck.size() == 52 }
    }

    @Test
    fun shuffleTest() {
        val deck = Deck()
        deck.shuffle()
        println(deck)
    }
}