package com.bitbucket.anuarkaliyev23.hundred.one.game

import com.bitbucket.anuarkaliyev23.hundred.one.card.Card
import com.bitbucket.anuarkaliyev23.hundred.one.card.Nominal
import com.bitbucket.anuarkaliyev23.hundred.one.card.Suit
import com.bitbucket.anuarkaliyev23.hundred.one.exceptions.InvalidPlayerMoveException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class PlayerMoveTest {

    @Test
    fun testException() {
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.TWO), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.THREE), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.FOUR), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.FIVE), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.SIX), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.SEVEN), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.EIGHT), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.NINE), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.TEN), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.JACK), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.KING), player = Player(), suit = Suit.DIAMONDS) }
        assertThrows<InvalidPlayerMoveException> { PlayerMove(card = Card(Suit.SPADES, Nominal.ACE), player = Player(), suit = Suit.DIAMONDS) }
        PlayerMove(card = Card(Suit.SPADES, Nominal.QUEEN), player = Player(), suit = Suit.CLUBS)
    }
}