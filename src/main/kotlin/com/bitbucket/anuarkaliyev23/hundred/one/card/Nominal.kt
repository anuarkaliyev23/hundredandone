package com.bitbucket.anuarkaliyev23.hundred.one.card

enum class Nominal(val points : Int, val symbol : String) {
    TWO(Nominal.TWO_POINTS, Nominal.TWO_SYMBOL),
    THREE(Nominal.THREE_POINTS, Nominal.THREE_SYMBOL),
    FOUR(Nominal.FOUR_POINTS, Nominal.FOUR_SYMBOL),
    FIVE(Nominal.FIVE_POINTS, Nominal.FIVE_SYMBOL),
    SIX(Nominal.SIX_POINTS, Nominal.SIX_SYMBOL),
    SEVEN(Nominal.SEVEN_POINTS, Nominal.SEVEN_SYMBOL),
    EIGHT(Nominal.EIGHT_POINTS, Nominal.EIGHT_SYMBOL),
    NINE(Nominal.NINE_POINTS, Nominal.NINE_SYMBOL),
    TEN(Nominal.TEN_POINTS, Nominal.TEN_SYMBOL),
    JACK(Nominal.JACK_POINTS, Nominal.JACK_SYMBOL),
    QUEEN(Nominal.QUEEN_POINTS, Nominal.QUEEN_SYMBOL),
    KING(Nominal.KING_POINTS, Nominal.KING_SYMBOL),
    ACE(Nominal.ACE_POINTS, Nominal.ACE_SYMBOL);

    companion object {
        private const val TWO_POINTS = 2
        private const val THREE_POINTS = 3
        private const val FOUR_POINTS = 4
        private const val FIVE_POINTS = 5
        private const val SIX_POINTS = 6
        private const val SEVEN_POINTS = 7
        private const val EIGHT_POINTS = 8
        private const val NINE_POINTS = 9
        private const val TEN_POINTS = 10
        private const val JACK_POINTS = 2
        private const val QUEEN_POINTS = 3
        private const val KING_POINTS = 4
        private const val ACE_POINTS = 11

        private const val TWO_SYMBOL = "2"
        private const val THREE_SYMBOL = "3"
        private const val FOUR_SYMBOL = "4"
        private const val FIVE_SYMBOL = "5"
        private const val SIX_SYMBOL = "6"
        private const val SEVEN_SYMBOL = "7"
        private const val EIGHT_SYMBOL = "8"
        private const val NINE_SYMBOL = "9"
        private const val TEN_SYMBOL = "10"
        private const val JACK_SYMBOL = "J"
        private const val QUEEN_SYMBOL = "Q"
        private const val KING_SYMBOL = "K"
        private const val ACE_SYMBOL = "A"
    }
}