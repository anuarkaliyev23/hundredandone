package com.bitbucket.anuarkaliyev23.hundred.one.exceptions

open class InvalidPlayerMoveException : GeneralHundredOneException()