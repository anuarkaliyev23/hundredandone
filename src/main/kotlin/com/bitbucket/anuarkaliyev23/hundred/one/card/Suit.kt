package com.bitbucket.anuarkaliyev23.hundred.one.card

enum class Suit(val symbol : String) {
    DIAMONDS(Suit.DIAMONDS_SYMBOL),
    CLUBS(Suit.CLUBS_SYMBOL),
    HEARTS(Suit.HEARTS_SYMBOL),
    SPADES(Suit.SPADES_SYMBOL);

    companion object {
        private const val DIAMONDS_SYMBOL = "\u2662"
        private const val CLUBS_SYMBOL = "\u2667"
        private const val HEARTS_SYMBOL = "\u2661"
        private const val SPADES_SYMBOL = "\u2664"
    }
}