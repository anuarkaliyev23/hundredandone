package com.bitbucket.anuarkaliyev23.hundred.one.game

import com.bitbucket.anuarkaliyev23.hundred.one.card.Card
import com.bitbucket.anuarkaliyev23.hundred.one.card.Deck
import com.bitbucket.anuarkaliyev23.hundred.one.exceptions.PlayerMoveOrderViolationException
import java.util.*

class HundredAndOne(val players : MutableList<Player> = ArrayList(),
                    val moveHistory : Deque<PlayerMove> = ArrayDeque(),
                    var move : Int = 0,
                    var deck : Deck = Deck()) {

    constructor(playersCount : Int) : this() {
        repeat(playersCount) {players.add(Player())}
    }

    fun activePlayer() : Player {
        return players[move % players.size]
    }

    fun move(playerMove : PlayerMove)  {
        if (playerMove.playerUUID != activePlayer().id) throw PlayerMoveOrderViolationException()
        deck.addCard(playerMove.card)
        moveHistory.addLast(playerMove)
        move++
    }

}