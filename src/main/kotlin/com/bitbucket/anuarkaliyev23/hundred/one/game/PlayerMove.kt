package com.bitbucket.anuarkaliyev23.hundred.one.game

import com.bitbucket.anuarkaliyev23.hundred.one.card.Card
import com.bitbucket.anuarkaliyev23.hundred.one.card.Nominal
import com.bitbucket.anuarkaliyev23.hundred.one.card.Suit
import com.bitbucket.anuarkaliyev23.hundred.one.exceptions.InvalidPlayerMoveException

data class PlayerMove(var card: Card, var playerUUID : String, var suit : Suit = card.suit) {
    init { if (suit != card.suit && card.nominal != Nominal.QUEEN) throw InvalidPlayerMoveException() }
}