package com.bitbucket.anuarkaliyev23.hundred.one.card

import java.util.*

class Deck() {
    private var cards : Deque<Card> = ArrayDeque()

    init {
        Nominal.values().forEach { nominal ->
            Suit.values().forEach { suit ->
                cards.add(Card(suit, nominal))
            }
        }
    }

    constructor(other : Deck) : this() {
        cards = ArrayDeque(other.cards)
    }

    fun shuffle() {
        cards = ArrayDeque(cards.shuffled())
    }

    fun card() : Card {
        return cards.removeFirst()
    }

    fun addCard(c : Card) {
        cards.addLast(c)
    }

    fun size(): Int {
        return cards.size
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Deck

        if (cards != other.cards) return false

        return true
    }

    override fun hashCode(): Int {
        return cards.hashCode()
    }

    override fun toString(): String {
        return "Deck$cards"
    }
}