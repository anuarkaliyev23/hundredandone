package com.bitbucket.anuarkaliyev23.hundred.one.game

import com.bitbucket.anuarkaliyev23.hundred.one.card.Card
import java.util.*

data class Player(var cards : List<Card>, var points : Int, var id : String) {
    constructor() : this(ArrayList<Card>(), 0, UUID.randomUUID().toString())
}