package com.bitbucket.anuarkaliyev23.hundred.one.card

data class Card(var suit : Suit, var nominal: Nominal) {
    fun isTrump() : Boolean = nominal == Nominal.QUEEN

    override fun toString(): String {
        return "${nominal.symbol}${suit.symbol}"
    }

}