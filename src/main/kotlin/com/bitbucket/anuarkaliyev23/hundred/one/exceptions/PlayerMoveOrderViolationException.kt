package com.bitbucket.anuarkaliyev23.hundred.one.exceptions

class PlayerMoveOrderViolationException : InvalidPlayerMoveException()